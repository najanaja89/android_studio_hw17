package com.example.hw17;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    private ImageView ballImage;
    private SensorManager sensorManager;
    private Sensor sensorAccelerometer;
    private Sensor sensorMagnet;

    private Timer timer;
    private int rotation;

    private Animation animation = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ballImage = findViewById(R.id.ballImageView);

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        sensorAccelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorMagnet = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(listener, sensorAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(listener, sensorMagnet, SensorManager.SENSOR_DELAY_NORMAL);

        timer = new Timer();
        TimerTask task1 = new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        getDeviceOrientation();
                        getActualDeviceOrientation();
                        showInfo();
                    }
                });
            }
        };
        timer.schedule(task1, 0, 3500);

        WindowManager windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        rotation = display.getRotation();
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(listener);
        timer.cancel();
    }


    private float[] valuesResult = new float[3];
    private float[] valuesResult2 = new float[3];

    private void showInfo() {

        if(valuesResult[0]==-0.0 && valuesResult[1]==-90 && valuesResult[2]==-0){
            Log.d("Hello", "-0 -90 -0");
            animation = new TranslateAnimation(0,0,0,900);
            AnimationSet set = new AnimationSet(true);
            animation.setDuration(1700);
            animation.setInterpolator(new AccelerateInterpolator());
            //animation.setRepeatMode(2);
            //animation.setRepeatCount(animation.getRepeatMode());
            animation.setFillAfter(true);
            set.addAnimation(animation);
            ballImage.startAnimation(animation);
        }
        else if(valuesResult[0]==-90  && valuesResult[2]==-90){
            Log.d("Hello", "-90 0 -90");
            animation = new TranslateAnimation(0,0,0,350);
            AnimationSet set = new AnimationSet(true);
            animation.setDuration(1700);
            animation.setInterpolator(new AccelerateInterpolator());
            //animation.setRepeatMode(2);
            //animation.setRepeatCount(animation.getRepeatMode());
            animation.setFillAfter(true);
            set.addAnimation(animation);
            ballImage.startAnimation(animation);
        }
        else if(valuesResult[0]==90 && valuesResult[2]==90){
            animation = new TranslateAnimation(0,0,0,350);
            AnimationSet set = new AnimationSet(true);
            animation.setDuration(1700);
            animation.setInterpolator(new AccelerateInterpolator());
            //animation.setRepeatMode(2);
            //animation.setRepeatCount(animation.getRepeatMode());
            animation.setFillAfter(true);
            set.addAnimation(animation);
            ballImage.startAnimation(animation);
        }
        //textView.setText(stringBuilder.toString());
    }

    float[] r = new float[9];
    private float[] valuesAccelerometer = new float[3];
    private float[] valuesMagnet = new float[3];

    private void getDeviceOrientation() {
        SensorManager.getRotationMatrix(r, null, valuesAccelerometer, valuesMagnet);
        SensorManager.getOrientation(r, valuesResult);

        valuesResult[0] = (float) Math.toDegrees(valuesResult[0]);
        valuesResult[1] = (float) Math.toDegrees(valuesResult[1]);
        valuesResult[2] = (float) Math.toDegrees(valuesResult[2]);
    }

    float[] inR = new float[9];
    float[] outR = new float[9];

    private void getActualDeviceOrientation() {
        SensorManager.getRotationMatrix(inR, null, valuesAccelerometer, valuesMagnet);
        int x_axis = SensorManager.AXIS_X;
        int y_axis = SensorManager.AXIS_Y;
        switch (rotation) {
            case Surface
                    .ROTATION_0:
                break;
            case Surface
                    .ROTATION_90:
                x_axis = SensorManager.AXIS_Y;
                y_axis = SensorManager.AXIS_MINUS_X;
                break;
            case Surface
                    .ROTATION_180:
                y_axis = SensorManager.AXIS_MINUS_Y;
                break;
            case Surface
                    .ROTATION_270:
                x_axis = SensorManager.AXIS_MINUS_Y;
                y_axis = SensorManager.AXIS_X;
                break;
            default:
                break;
        }

        SensorManager.remapCoordinateSystem(inR, x_axis, y_axis, outR);
        SensorManager.getOrientation(outR, valuesResult2);
        valuesResult2[0] = (float) Math.toDegrees(valuesResult2[0]);
        valuesResult2[1] = (float) Math.toDegrees(valuesResult2[1]);
        valuesResult2[2] = (float) Math.toDegrees(valuesResult2[2]);
    }

    private SensorEventListener listener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            switch (event.sensor.getType()) {
                case Sensor.TYPE_ACCELEROMETER:
                    for (int i = 0; i < 3; i++) {
                        valuesAccelerometer[i] = event.values[i];
                    }
                    break;
                case Sensor.TYPE_MAGNETIC_FIELD:
                    for (int i = 0; i < 3; i++) {
                        valuesMagnet[i] = event.values[i];
                    }
                    break;
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    };

}